# Friulian translation for gnome-contacts.
# Copyright (C) 2013 gnome-contacts's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-contacts package.
# Fabio Tomat <f.t.public@gmail.com>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-contacts master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-contacts/issues\n"
"POT-Creation-Date: 2023-02-24 15:03+0000\n"
"PO-Revision-Date: 2023-02-25 07:43+0000\n"
"Last-Translator: Fabio T. <f.t.public@gmail.com>\n"
"Language-Team: Friulian <f.t.public@gmail.com>\n"
"Language: fur\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Editor: HaiPO 1.4 beta\n"
"X-Generator: Poedit 3.1.1\n"

#: data/org.gnome.Contacts.appdata.xml.in.in:6
#: data/org.gnome.Contacts.desktop.in.in:3 data/ui/contacts-main-window.ui:131
#: src/contacts-main-window.vala:200 src/contacts-main-window.vala:223
#: src/main.vala:26
msgid "Contacts"
msgstr "Contats"

#: data/org.gnome.Contacts.appdata.xml.in.in:7
#: data/org.gnome.Contacts.desktop.in.in:4
msgid "A contacts manager for GNOME"
msgstr "Un gjestôr contats par GNOME"

#: data/org.gnome.Contacts.appdata.xml.in.in:9
msgid ""
"Contacts keeps and organize your contacts information. You can create, edit,"
" delete and link together pieces of information about your contacts. "
"Contacts aggregates the details from all your sources providing a "
"centralized place for managing your contacts."
msgstr ""
"Contats al manten e al ministre lis informazions dai tiei contats. Tu puedis"
" creâ, modificâ, scancelâ e leâ adun tocs di informazions dai tiei contats. "
"Contats al unìs i detais di dutis lis tôs sorzints dant un sisteme "
"centralizât par ministrâ i tiei contats."

#: data/org.gnome.Contacts.appdata.xml.in.in:15
msgid ""
"Contacts will also integrate with online address books and automatically "
"link contacts from different online sources."
msgstr ""
"In plui a vegnaran integradis lis rubrichis in-linie e a vegnaran unîts in "
"maniere automatiche i contats di diviersis sorzints in-linie."

#: data/org.gnome.Contacts.appdata.xml.in.in:22
msgid "Contacts with no contacts"
msgstr "Contats cence contats"

#: data/org.gnome.Contacts.appdata.xml.in.in:26
msgid "Contacts filled with contacts"
msgstr "Contats jemplât cui contats"

#: data/org.gnome.Contacts.appdata.xml.in.in:30
msgid "Contacts in selection mode"
msgstr "Contats in modalitât selezion"

#: data/org.gnome.Contacts.appdata.xml.in.in:34
msgid "Contacts setup view"
msgstr "Viodude configurazion di Contats"

#: data/org.gnome.Contacts.appdata.xml.in.in:38
msgid "Contacts edit view"
msgstr "Viodude modifiche di Contats"

#: data/org.gnome.Contacts.appdata.xml.in.in:829 src/contacts-app.vala:144
msgid "The GNOME Project"
msgstr "Il progjet GNOME"

#. Translators: Search terms to find this application. Do NOT translate or
#. localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Contacts.desktop.in.in:6
msgid "friends;address book;"
msgstr "amîs;rubriche;"

#: data/gtk/help-overlay.ui:7
msgctxt "shortcut window"
msgid "Overview"
msgstr "Panoramiche"

#: data/gtk/help-overlay.ui:11
msgctxt "shortcut window"
msgid "Help"
msgstr "Jutori"

#: data/gtk/help-overlay.ui:17
msgctxt "shortcut window"
msgid "Open menu"
msgstr "Vierç menù"

#: data/gtk/help-overlay.ui:23
msgctxt "shortcut window"
msgid "Show preferences"
msgstr "Mostre lis preferencis"

#: data/gtk/help-overlay.ui:29
msgctxt "shortcut window"
msgid "Create a new contact"
msgstr "Cree un gnûf contat"

#: data/gtk/help-overlay.ui:35
msgctxt "shortcut window"
msgid "Search contacts"
msgstr "Cîr contats"

#: data/gtk/help-overlay.ui:41
msgctxt "shortcut window"
msgid "Shortcut list"
msgstr "Liste scurtis"

#: data/gtk/help-overlay.ui:47
msgctxt "shortcut window"
msgid "Quit"
msgstr "Jes"

#: data/gtk/help-overlay.ui:54
msgctxt "shortcut window"
msgid "Editing or creating a contact"
msgstr "Daûr a modificâ o creâ un contat"

#: data/gtk/help-overlay.ui:58
msgctxt "shortcut window"
msgid "Save current changes to contact"
msgstr "Salve lis modifichis atuâls al contat"

#: data/gtk/help-overlay.ui:64
msgctxt "shortcut window"
msgid "Cancel current changes for contact"
msgstr "Anule lis modifichis atuâls pal contat"

#: data/ui/contacts-avatar-selector.ui:28 data/ui/contacts-crop-dialog.ui:25
#: data/ui/contacts-main-window.ui:286 src/contacts-app.vala:234
#: src/contacts-app.vala:395 src/contacts-avatar-selector.vala:211
#: src/contacts-contact-editor.vala:714 src/contacts-main-window.vala:551
#: src/contacts-main-window.vala:607
msgid "_Cancel"
msgstr "_Anule"

#: data/ui/contacts-avatar-selector.ui:37
msgid "Select a new avatar"
msgstr "Selezione un gnûf avatar"

#: data/ui/contacts-avatar-selector.ui:43 data/ui/contacts-crop-dialog.ui:40
#: data/ui/contacts-setup-window.ui:29
msgid "_Done"
msgstr "_Fat"

#: data/ui/contacts-avatar-selector.ui:94
msgid "Take a Picture…"
msgstr "Scate une foto…"

#: data/ui/contacts-avatar-selector.ui:101
msgid "Select a File…"
msgstr "Selezione un file…"

#: data/ui/contacts-contact-pane.ui:14
msgid "Select a Contact"
msgstr "Selezione un contat"

#: data/ui/contacts-editor-menu.ui:11
msgid "Change Addressbook"
msgstr "Change Addressbook"

#: data/ui/contacts-editable-avatar.ui:17
msgid "Change Avatar"
msgstr "Cambie avatar"

#: data/ui/contacts-editable-avatar.ui:36
msgid "Remove Avatar"
msgstr "Gjave avatar"

#: data/ui/contacts-link-suggestion-grid.ui:51
msgid "Link Contacts"
msgstr "Lee i contats"

#: data/ui/contacts-main-window.ui:5
msgid "List Contacts By:"
msgstr "Liste i contats par:"

#: data/ui/contacts-main-window.ui:7
msgid "First Name"
msgstr "Non"

#: data/ui/contacts-main-window.ui:12
msgid "Surname"
msgstr "Cognon"

#: data/ui/contacts-main-window.ui:19
msgid "Import From File…"
msgstr "Impuarte di file…"

#: data/ui/contacts-main-window.ui:23
msgid "Export All Contacts…"
msgstr "Espuarte ducj i contats…"

#: data/ui/contacts-main-window.ui:29
msgid "Preferences"
msgstr "Preferencis"

#: data/ui/contacts-main-window.ui:33
msgid "Keyboard Shortcuts"
msgstr "Scurtis di tastiere"

#: data/ui/contacts-main-window.ui:37
msgid "Help"
msgstr "Jutori"

#: data/ui/contacts-main-window.ui:41
msgid "About Contacts"
msgstr "Informazions su Contats"

#: data/ui/contacts-main-window.ui:54
msgid "Share as QR Code"
msgstr "Condivît come codiç QR"

#: data/ui/contacts-main-window.ui:60
msgid "Delete Contact"
msgstr "Elimine il contat"

#: data/ui/contacts-main-window.ui:123
msgid "Add New Contact"
msgstr "Zonte un gnûf contat"

#: data/ui/contacts-main-window.ui:139
msgid "Main Menu"
msgstr "Menù principâl"

#: data/ui/contacts-main-window.ui:146
msgid "Select Contacts"
msgstr "Selezione i contats"

#: data/ui/contacts-main-window.ui:153 src/contacts-app.vala:329
msgid "Cancel"
msgstr "Anule"

#: data/ui/contacts-main-window.ui:154
msgid "Cancel Selection"
msgstr "Anule selezion"

#: data/ui/contacts-main-window.ui:181
msgid "Loading"
msgstr "Cjariament in vore"

#: data/ui/contacts-main-window.ui:192
msgid "Search contacts"
msgstr "Cîr contats"

#. Export refers to the verb
#: data/ui/contacts-main-window.ui:210
msgid "Export"
msgstr "Espuarte"

#: data/ui/contacts-main-window.ui:211
msgid "Export Selected Contacts"
msgstr "Espuarte i contats selezionâts"

#. Link refers to the verb, from linking contacts together
#: data/ui/contacts-main-window.ui:218
msgid "Link"
msgstr "Unìs"

#: data/ui/contacts-main-window.ui:219
msgid "Link Selected Contacts Together"
msgstr "Coleghe i contats selezionâts adun"

#: data/ui/contacts-main-window.ui:226
msgid "Remove"
msgstr "Gjave"

#: data/ui/contacts-main-window.ui:272
msgid "Back"
msgstr "Indaûr"

#: data/ui/contacts-main-window.ui:301
msgid "Edit Contact"
msgstr "Modifiche il contat"

#: data/ui/contacts-main-window.ui:308
msgid "More Contact Actions"
msgstr "Azions adizionâls sui contats"

#: data/ui/contacts-main-window.ui:318 src/contacts-main-window.vala:232
msgid "Done"
msgstr "Fat"

#: data/ui/contacts-preferences-window.ui:6
msgid "Address Books"
msgstr "Rubrichis"

#: data/ui/contacts-qr-code-dialog.ui:9
msgid "Share Contact"
msgstr "Condivît contat"

#: data/ui/contacts-qr-code-dialog.ui:36
msgid "QR Code"
msgstr "Codiç QR"

#: data/ui/contacts-qr-code-dialog.ui:46
msgid "Scan to Save"
msgstr "Scansione par salvâ"

#: data/ui/contacts-setup-window.ui:15
msgid "Contacts Setup"
msgstr "Configurazion di Contats"

#: data/ui/contacts-setup-window.ui:20
msgid "_Quit"
msgstr "J_es"

#: data/ui/contacts-setup-window.ui:22
msgid "Cancel Setup And Quit"
msgstr "Anule la configurazion e jes"

#. Translators: "Complete" is a verb here: a user finishes the setup by
#. clicking this button
#: data/ui/contacts-setup-window.ui:31
msgid "Complete setup"
msgstr "Configurazion completade"

#: data/ui/contacts-setup-window.ui:42
msgid "Welcome"
msgstr "Benvignût"

#: data/ui/contacts-setup-window.ui:43
msgid ""
"Please select your main address book: this is where new contacts will be "
"added. If you keep your contacts in an online account, you can add them "
"using the online accounts settings."
msgstr ""
"Selezione la tô rubriche principâl: chê e sarà la rubriche dulà che a "
"vignaran zontâts i gnûfs contats. Se si ten i contats intune rubriche in "
"rêt, si pues zontâju doprant lis impostazions dai account online."

#: src/contacts-app.vala:46
msgid "Show contact with this email address"
msgstr "Mostre contat cun chest recapit e-mail"

#: src/contacts-app.vala:47
msgid "Show contact with this individual id"
msgstr "Mostre contat cun chest ID"

#: src/contacts-app.vala:48
msgid "Show contacts with the given filter"
msgstr "Mostre i contats cul filtri indicât"

#: src/contacts-app.vala:49
msgid "Show the current version of Contacts"
msgstr "Mostre la version atuâl di Contats"

#: src/contacts-app.vala:111 src/contacts-app.vala:164
msgid "Contact not found"
msgstr "Contat no cjatât"

#: src/contacts-app.vala:112
#, c-format
msgid "No contact with id %s found"
msgstr "Nol è stât cjatât nissun contat cun ID %s"

#: src/contacts-app.vala:113 src/contacts-app.vala:166
#: src/contacts-avatar-selector.vala:173 src/contacts-avatar-selector.vala:250
msgid "_Close"
msgstr "_Siere"

#: src/contacts-app.vala:150
msgid ""
"© 2011 Red Hat, Inc.\n"
"© 2011-2020 The Contacts Developers"
msgstr ""
"© 2011 Red Hat, Inc.\n"
"© 2011-2020 I disvilupadôrs di Contats"

#: src/contacts-app.vala:165
#, c-format
msgid "No contact with email address %s found"
msgstr "Nol è stât cjatât nissun contat cun recapit e-mail %s"

#: src/contacts-app.vala:228
msgid "Primary address book not found"
msgstr "Rubriche primarie no cjatade"

#: src/contacts-app.vala:230
msgid ""
"Contacts can't find the configured primary address book. You might "
"experience issues creating or editing contacts"
msgstr ""
"Contats nol rive a cjatâ la rubriche primarie configurade. Tu podaressis "
"cjatâ problemis te creazion o te modifiche dai contats"

#: src/contacts-app.vala:231
msgid "Go To _Preferences"
msgstr "Va aes _Preferncis"

#: src/contacts-app.vala:325
msgid "Select contact file"
msgstr "Selezione file di contat"

#: src/contacts-app.vala:328
msgid "Import"
msgstr "Impuarte"

#. TODO: somehow get this from the list of importers we have
#: src/contacts-app.vala:335
msgid "vCard files"
msgstr "Files vCard"

#: src/contacts-app.vala:368
msgid "Error reading file"
msgstr "Erôr tal lei il file"

#: src/contacts-app.vala:369
#, c-format
msgid "An error occurred reading the file '%s'"
msgstr "Al è capitât un erôr tal lei il file '%s'"

#: src/contacts-app.vala:370 src/contacts-app.vala:380
msgid "_OK"
msgstr "_Va ben"

#: src/contacts-app.vala:378
msgid "No contacts found"
msgstr "Nissun contat cjatât"

#: src/contacts-app.vala:379
msgid "The imported file does not seem to contain any contacts"
msgstr "Il file impuartât nol somee contignî nissun contat"

#. Second step: ask the user for confirmation
#: src/contacts-app.vala:387
#, c-format
msgid "By continuing, you will import %u contact"
msgid_plural "By continuing, you will import %u contacts"
msgstr[0] "Continuant tu impuartarâs %u contat"
msgstr[1] "Continuant tu impuartarâs %u contats"

#: src/contacts-app.vala:390
msgid "Continue Import?"
msgstr "Continuâ a impuartâ?"

#: src/contacts-app.vala:391
msgid "C_ontinue"
msgstr "C_ontinue"

#: src/contacts-avatar-selector.vala:126
msgid "No Camera Detected"
msgstr "Nissune videocjamare rilevade"

#: src/contacts-avatar-selector.vala:172
msgid "Failed to set avatar"
msgstr "Nol è stât pussibil configurâ l'avatar"

#: src/contacts-avatar-selector.vala:208
msgid "Browse for more pictures"
msgstr "Cîr plui imagjins"

#: src/contacts-avatar-selector.vala:211
msgid "_Open"
msgstr "_Vierç"

#: src/contacts-avatar-selector.vala:249
msgid "Failed to set avatar."
msgstr "No si è rivâts a stabilî l'avatar."

#: src/contacts-contact-editor.vala:198
msgid "Show More"
msgstr "Mostre di plui"

#: src/contacts-contact-editor.vala:295
msgid "Add email"
msgstr "Zonte e-mail"

#: src/contacts-contact-editor.vala:321
msgid "Add phone number"
msgstr "Zonte numar di telefon"

#: src/contacts-contact-editor.vala:347 src/contacts-utils.vala:188
msgid "Website"
msgstr "Sît web"

#: src/contacts-contact-editor.vala:358 src/contacts-utils.vala:173
msgid "Full name"
msgstr "Non complet"

#: src/contacts-contact-editor.vala:369 src/contacts-utils.vala:179
msgid "Nickname"
msgstr "Sorenon"

#: src/contacts-contact-editor.vala:420 src/contacts-utils.vala:170
msgid "Birthday"
msgstr "Complean"

#: src/contacts-contact-editor.vala:441
msgid "Remove birthday"
msgstr "Gjave complean"

#: src/contacts-contact-editor.vala:459
msgid "Set Birthday"
msgstr "Stabilìs complean"

#: src/contacts-contact-editor.vala:513
msgid "Organisation"
msgstr "Organizazion"

#. TRANSLATORS: This is the role of a contact in an organisation (e.g. CEO)
#: src/contacts-contact-editor.vala:519 src/contacts-utils.vala:184
msgid "Role"
msgstr "Rûl"

#: src/contacts-contact-editor.vala:635
msgid "Label"
msgstr "Etichete"

#. Create grid and labels
#: src/contacts-contact-editor.vala:695
msgid "Day"
msgstr "Dì"

#: src/contacts-contact-editor.vala:699
msgid "Month"
msgstr "Mês"

#: src/contacts-contact-editor.vala:703
msgid "Year"
msgstr "An"

#: src/contacts-contact-editor.vala:711
msgid "Change Birthday"
msgstr "Cambie complean"

#: src/contacts-contact-editor.vala:718
msgid "_Set"
msgstr "_Stabilìs"

#: src/contacts-contact-editor.vala:779
msgid "Street"
msgstr "Vie"

#: src/contacts-contact-editor.vala:779
msgid "Extension"
msgstr "Civic"

#: src/contacts-contact-editor.vala:779
msgid "City"
msgstr "Citât"

#: src/contacts-contact-editor.vala:779
msgid "State/Province"
msgstr "Stât/Province"

#: src/contacts-contact-editor.vala:779
msgid "Zip/Postal Code"
msgstr "ZIP/Codiç Postâl"

#: src/contacts-contact-editor.vala:779
msgid "PO box"
msgstr "Casele di pueste"

#: src/contacts-contact-editor.vala:779
msgid "Country"
msgstr "Paîs"

#: src/contacts-contact-list.vala:115
msgid "Favorites"
msgstr "Preferîts"

#: src/contacts-contact-list.vala:117 src/contacts-contact-list.vala:123
msgid "All Contacts"
msgstr "Ducj i contats"

#: src/contacts-contact-sheet.vala:215
#, c-format
msgid "Send an email to %s"
msgstr "Invie une e-mail a %s"

#: src/contacts-contact-sheet.vala:288
msgid "Visit website"
msgstr "Visite il sît web"

#: src/contacts-contact-sheet.vala:321
msgid "Their birthday is today! 🎉"
msgstr "Il lôr complean al è vuê! 🎉"

#: src/contacts-contact-sheet.vala:368
msgid "Show on the map"
msgstr "Mostre te mape"

#: src/contacts-delete-operation.vala:36
#, c-format
msgid "Deleting %d contact"
msgid_plural "Deleting %d contacts"
msgstr[0] "Daûr a eliminâ %d contat"
msgstr[1] "Daûr a eliminâ %d contats"

#. Special-case the local address book
#: src/contacts-esd-setup.vala:152 src/contacts-utils.vala:119
msgid "Local Address Book"
msgstr "Rubriche locâl"

#: src/contacts-esd-setup.vala:155 src/contacts-esd-setup.vala:170
#: src/contacts-utils.vala:149
msgid "Google"
msgstr "Google"

#: src/contacts-esd-setup.vala:167
msgid "Local Contact"
msgstr "Contat locâl"

#: src/contacts-im-service.vala:30
msgid "AOL Instant Messenger"
msgstr "AOL Instant Messenger"

#: src/contacts-im-service.vala:31
msgid "Facebook"
msgstr "Facebook"

#: src/contacts-im-service.vala:32
msgid "Gadu-Gadu"
msgstr "Gadu-Gadu"

#: src/contacts-im-service.vala:33
msgid "Google Talk"
msgstr "Google Talk"

#: src/contacts-im-service.vala:34
msgid "Novell Groupwise"
msgstr "Novell Groupwise"

#: src/contacts-im-service.vala:35
msgid "ICQ"
msgstr "ICQ"

#: src/contacts-im-service.vala:36
msgid "IRC"
msgstr "IRC"

#: src/contacts-im-service.vala:37
msgid "Jabber"
msgstr "Jabber"

#: src/contacts-im-service.vala:38
msgid "Livejournal"
msgstr "Livejournal"

#: src/contacts-im-service.vala:39
msgid "Local network"
msgstr "Rêt locâl"

#: src/contacts-im-service.vala:40
msgid "Windows Live Messenger"
msgstr "Windows Live Messenger"

#: src/contacts-im-service.vala:41
msgid "MySpace"
msgstr "MySpace"

#: src/contacts-im-service.vala:42
msgid "MXit"
msgstr "MXit"

#: src/contacts-im-service.vala:43
msgid "Napster"
msgstr "Napster"

#: src/contacts-im-service.vala:44
msgid "Ovi Chat"
msgstr "Ovi Chat"

#: src/contacts-im-service.vala:45
msgid "Tencent QQ"
msgstr "Tencent QQ"

#: src/contacts-im-service.vala:46
msgid "IBM Lotus Sametime"
msgstr "IBM Lotus Sametime"

#: src/contacts-im-service.vala:47
msgid "SILC"
msgstr "SILC"

#: src/contacts-im-service.vala:48
msgid "sip"
msgstr "sip"

#: src/contacts-im-service.vala:49
msgid "Skype"
msgstr "Skype"

#: src/contacts-im-service.vala:50
msgid "Telephony"
msgstr "Telefonie"

#: src/contacts-im-service.vala:51
msgid "Trepia"
msgstr "Trepia"

#: src/contacts-im-service.vala:52 src/contacts-im-service.vala:53
msgid "Yahoo! Messenger"
msgstr "Yahoo! Messenger"

#: src/contacts-im-service.vala:54
msgid "Zephyr"
msgstr "Zephyr"

#: src/contacts-link-operation.vala:38
#, c-format
msgid "Linked %d contact"
msgid_plural "Linked %d contacts"
msgstr[0] "Leât %d contat"
msgstr[1] "Leâts %d contats"

#: src/contacts-link-suggestion-grid.vala:49
#, c-format
msgid "Is this the same person as %s from %s?"
msgstr "Ise cheste la stesse persone di %s di %s?"

#: src/contacts-link-suggestion-grid.vala:52
#, c-format
msgid "Is this the same person as %s?"
msgstr "Ise la stesse persone di %s?"

#: src/contacts-main-window.vala:145 src/contacts-main-window.vala:489
msgid "Mark as Favorite"
msgstr "Segne come preferît"

#: src/contacts-main-window.vala:202
#, c-format
msgid "%llu Selected"
msgid_plural "%llu Selected"
msgstr[0] "%llu Selezionât"
msgstr[1] "%llu Selezionâts"

#: src/contacts-main-window.vala:232
msgid "_Add"
msgstr "_Zonte"

#: src/contacts-main-window.vala:262
#, c-format
msgid "Editing %s"
msgstr "Modifiche di %s"

#: src/contacts-main-window.vala:290
#, c-format
msgid "%d Selected"
msgid_plural "%d Selected"
msgstr[0] "%d Selezionât"
msgstr[1] "%d Selezionâts"

#: src/contacts-main-window.vala:310 src/contacts-main-window.vala:522
#: src/contacts-main-window.vala:562
msgid "_Undo"
msgstr "_Torne indaûr"

#: src/contacts-main-window.vala:396
msgid "New Contact"
msgstr "Gnûf contat"

#: src/contacts-main-window.vala:487
msgid "Unmark as Favorite"
msgstr "Gjave segn di preferît"

#. Open up a file chooser
#: src/contacts-main-window.vala:603
msgid "Export to file"
msgstr "Espuarte su file"

#: src/contacts-main-window.vala:606
msgid "_Export"
msgstr "_Espuarte"

#: src/contacts-main-window.vala:608
msgid "contacts.vcf"
msgstr "contacts.vcf"

#: src/contacts-preferences-window.vala:30
msgid "Primary Address Book"
msgstr "Rubriche primarie"

#: src/contacts-preferences-window.vala:31
msgid ""
"New contacts will be added to the selected address book. You are able to "
"view and edit contacts from other address books."
msgstr ""
"I gnûfs contats a vegnaran zontâts inte rubriche selezionade. Tu podarâs in "
"ogni câs viodi e modificâ i contats di altris rubrichis."

#: src/contacts-preferences-window.vala:40
msgid "_Online Accounts"
msgstr "Accounts _online"

#: src/contacts-preferences-window.vala:45
msgid "Open the Online Accounts panel in Settings"
msgstr "Vierç il panel dai Accounts online tes Impostazions"

#: src/contacts-qr-code-dialog.vala:33
#, c-format
msgid "Scan the QR code to save the contact <b>%s</b>."
msgstr "Scansione il codiç QR par salvâ il contat <b>%s</b>."

#: src/contacts-unlink-operation.vala:37
msgid "Unlinking contacts"
msgstr "Daûr a disleâ i contats"

#: src/contacts-utils.vala:168
msgid "Alias"
msgstr "Pseudonim"

#: src/contacts-utils.vala:169
msgid "Avatar"
msgstr "Avatar"

#: src/contacts-utils.vala:171
msgid "Calendar event"
msgstr "Event di calendari"

#: src/contacts-utils.vala:172
msgid "Email address"
msgstr "Direzion e-mail"

#: src/contacts-utils.vala:174
msgid "Gender"
msgstr "Gjenar"

#: src/contacts-utils.vala:175
msgid "Group"
msgstr "Grup"

#: src/contacts-utils.vala:176
msgid "Instant messaging"
msgstr "Messaçs istantanis"

#: src/contacts-utils.vala:177
msgid "Favourite"
msgstr "Preferît"

#: src/contacts-utils.vala:178
msgid "Local ID"
msgstr "ID locâl"

#: src/contacts-utils.vala:180
msgid "Note"
msgstr "Note"

#: src/contacts-utils.vala:181
msgid "Phone number"
msgstr "Numar di telefon"

#: src/contacts-utils.vala:182
msgid "Address"
msgstr "Recapit"

#. TRANSLATORS: This is a field which contains a name decomposed in several
#. parts, rather than a single freeform string for the full name
#: src/contacts-utils.vala:187
msgid "Structured name"
msgstr "Non struturât"

#: src/contacts-utils.vala:189
msgid "Web service"
msgstr "Servizi web"

#: src/core/contacts-contact.vala:38
msgid "Unnamed Person"
msgstr "Persone cence non"

#. TRANSLATORS: "$ROLE at $ORGANISATION", e.g. "CEO at Linux Inc."
#: src/core/contacts-roles-chunk.vala:105
#, c-format
msgid "%s at %s"
msgstr "%s di %s"

#: src/core/contacts-type-descriptor.vala:82
#: src/core/contacts-type-set.vala:232
msgid "Other"
msgstr "Altri"

#. List most specific first, always in upper case
#: src/core/contacts-type-set.vala:180 src/core/contacts-type-set.vala:201
#: src/core/contacts-type-set.vala:227 src/core/contacts-type-set.vala:229
msgid "Home"
msgstr "Cjase"

#: src/core/contacts-type-set.vala:181 src/core/contacts-type-set.vala:202
#: src/core/contacts-type-set.vala:221 src/core/contacts-type-set.vala:223
msgid "Work"
msgstr "Vore"

#. List most specific first, always in upper case
#: src/core/contacts-type-set.vala:200
msgid "Personal"
msgstr "Personâl"

#. List most specific first, always in upper case
#: src/core/contacts-type-set.vala:220
msgid "Assistant"
msgstr "Assistent"

#: src/core/contacts-type-set.vala:222
msgid "Work Fax"
msgstr "Fax di vore"

#: src/core/contacts-type-set.vala:224
msgid "Callback"
msgstr "Riclamade"

#: src/core/contacts-type-set.vala:225
msgid "Car"
msgstr "Machine"

#: src/core/contacts-type-set.vala:226
msgid "Company"
msgstr "Societât"

#: src/core/contacts-type-set.vala:228
msgid "Home Fax"
msgstr "Fax di cjase"

#: src/core/contacts-type-set.vala:230
msgid "ISDN"
msgstr "ISDN"

#: src/core/contacts-type-set.vala:231
msgid "Mobile"
msgstr "Celulâr"

#: src/core/contacts-type-set.vala:233
msgid "Fax"
msgstr "Fax"

#: src/core/contacts-type-set.vala:234
msgid "Pager"
msgstr "Ciridôr"

#: src/core/contacts-type-set.vala:235
msgid "Radio"
msgstr "Radio"

#: src/core/contacts-type-set.vala:236
msgid "Telex"
msgstr "Telex"

#. To translators: TTY is Teletypewriter
#: src/core/contacts-type-set.vala:238
msgid "TTY"
msgstr "TTY"

#: src/io/contacts-io-parse-operation.vala:40
#, c-format
msgid "Importing contacts from '%s'"
msgstr "Importazion contats di '%s'"

#: src/org.gnome.Contacts.gschema.xml:6
msgid "First-time setup done."
msgstr "Impostazions di prin inviament completadis."

#: src/org.gnome.Contacts.gschema.xml:7
msgid "Set to true after the user has run the first-time setup wizard."
msgstr ""
"Met a vêr dopo che l'utent al à eseguît l'assistent ae configurazion "
"iniziâl."

#: src/org.gnome.Contacts.gschema.xml:11
msgid "Sort contacts on surname."
msgstr "Ordene i contats par cognon."

#: src/org.gnome.Contacts.gschema.xml:12
msgid ""
"If this is set to true, the list of contacts will be sorted on their "
"surnames. Otherwise, it will be sorted on the first names of the contacts."
msgstr ""
"Se chest al è metût a vêr, la liste dai contats e vignarà ordenade in base "
"ai cognons. In câs contrari, e vignarà ordenade par non dai contats."

#: src/org.gnome.Contacts.gschema.xml:19
msgid "The default height of the contacts window."
msgstr "La altece predefinide dal barcon dai contats."

#: src/org.gnome.Contacts.gschema.xml:20
msgid ""
"If the window size has not been changed by the user yet this will be used as"
" the initial value for the height of the window."
msgstr ""
"Se la dimension dal barcon no je ancjemò stade cambiade dal utent chest al "
"vignarà doprât come valôr iniziâl pe altece dal barcon."

#: src/org.gnome.Contacts.gschema.xml:26
msgid "The default width of the contacts window."
msgstr "La largjece predefinide dal barcon dai contats."

#: src/org.gnome.Contacts.gschema.xml:27
msgid ""
"If the window size has not been changed by the user yet this will be used as"
" the initial value for the width of the window."
msgstr ""
"Se la dimension dal barcon no je ancjemò stade cambiade dal utent chest al "
"vignarà doprât come valôr iniziâl pe largjece dal barcon."

#: src/org.gnome.Contacts.gschema.xml:32
msgid "Is the window maximized?"
msgstr "Isal il barcon slargjât?"

#: src/org.gnome.Contacts.gschema.xml:33 src/org.gnome.Contacts.gschema.xml:38
msgid "Stores if the window is currently maximized."
msgstr "Al archivie se il barcon al è cumò slargjât."

#: src/org.gnome.Contacts.gschema.xml:37
msgid "Is the window fullscreen"
msgstr "Isal il barcon a plen visôr"

#~ msgctxt "shortcut window"
#~ msgid "Search"
#~ msgstr "Cîr"

#~ msgid "Linked Accounts"
#~ msgstr "Account leâts"

#~ msgid "You can link contacts by selecting them from the contacts list"
#~ msgstr "Al è pussibil unî i contats selezionantju de liste dai contats"

#~ msgid "Create new contact"
#~ msgstr "Cree gnûf contat"

#~ msgid "Menu"
#~ msgstr "Menù"

#~ msgid "Select Items"
#~ msgstr "Selezione elements"

#~ msgid "Type to search"
#~ msgstr "Scrîf par cirî"

#~ msgid "Add name"
#~ msgstr "Zonte non"

#~ msgid "Set"
#~ msgstr "Stabilìs"

#~ msgid "Unlink"
#~ msgstr "Separe"

#~ msgid "Change Address Book…"
#~ msgstr "Cambie rubriche…"

#~ msgid "Online Accounts <sup>↗</sup>"
#~ msgstr "Account online <sup>↗</sup>"

#~ msgid "Change Address Book"
#~ msgstr "Cambie rubriche"

#~ msgid "Change"
#~ msgstr "Cambie"

#~ msgid ""
#~ "New contacts will be added to the selected address book.\n"
#~ "You are able to view and edit contacts from other address books."
#~ msgstr ""
#~ "I gnûfs contats a vegnaran zontâts inte rubriche selezionade.\n"
#~ "Tu podarâs in ogni câs viodi e modificâ i contats di altris rubrichis."

#~ msgid "translator-credits"
#~ msgstr "Fabio Tomat <f.t.public@gmail.com>, 2022"

#~ msgid "About GNOME Contacts"
#~ msgstr "Informazions su Contats di GNOME"

#~ msgid "Contact Management Application"
#~ msgstr "Aplicazion par ministrâ i contats"

#, c-format
#~ msgid "Unable to create new contacts: %s"
#~ msgstr "Impussibil creâ gnûfs contats: %s"

#~ msgid "Unable to find newly created contact"
#~ msgstr "Impussibil cjatâ il contat creât cumò denant"

#~ msgid "Start a call"
#~ msgstr "Tache une clamade"

#~ msgid "Delete field"
#~ msgstr "Elimine cjamp"

#~ msgid "https://example.com"
#~ msgstr "https://esempli.com"

#~ msgid "Take Another…"
#~ msgstr "Scate une altre…"

#~ msgid "Share"
#~ msgstr "Condivît"

#~ msgid "Edit"
#~ msgstr "Cambie"

#~ msgid "Delete"
#~ msgstr "Elimine"

#~ msgid "Add contact"
#~ msgstr "Zonte contat"

#~ msgid "Setup complete"
#~ msgstr "Impostazions completadis"

#~ msgid "GNOME Contacts"
#~ msgstr "Contats di GNOME"

#~ msgid "Unable to take photo."
#~ msgstr "Impussibil scatâ la foto."

#~ msgid "Add number"
#~ msgstr "Zonte numar"

#~ msgid "%d contacts linked"
#~ msgid_plural "%d contacts linked"
#~ msgstr[0] "%d contat unît"
#~ msgstr[1] "%d contats unîts"

#~ msgid "%d contact deleted"
#~ msgid_plural "%d contacts deleted"
#~ msgstr[0] "%d contat eliminât"
#~ msgstr[1] "%d contats eliminâts"

#~ msgid "%s linked to %s"
#~ msgstr "%s unît cun %s"

#~ msgid "Google Circles"
#~ msgstr "Cerclis di Google"

#~ msgid "Add"
#~ msgstr "Zonte"

#~ msgid "Home email"
#~ msgstr "E-mail di cjase"

#~ msgid "Work email"
#~ msgstr "E-mail di vore"

#~ msgid "Mobile phone"
#~ msgstr "Celulâr"

#~ msgid "Home phone"
#~ msgstr "Telefon di cjase "

#~ msgid "Work phone"
#~ msgstr "Telefon di vore"

#~ msgid "Work address"
#~ msgstr "Recapit di vore"

#~ msgid "Notes"
#~ msgstr "Notis"

#~ msgid "New Detail"
#~ msgstr "Gnûf detai"

#~ msgid "Edit details"
#~ msgstr "Modifiche detais"

#~ msgid "You need to enter some data"
#~ msgstr "Tu scugnis meti cualchi dât"

#~ msgid "Unexpected internal error: created contact was not found"
#~ msgstr "Erôr interni no spietât: il contat creât nol è stât cjatât"

#~ msgid "org.gnome.Contacts"
#~ msgstr "org.gnome.Contacts"

#~ msgid "Quit"
#~ msgstr "Jes"

#~ msgid "_Help"
#~ msgstr "_Jutori"

#~ msgid "_About"
#~ msgstr "_Informazions"

#~ msgid "Selection mode"
#~ msgstr "Modalitât di selezion"

#~ msgid "Install GNOME Maps to open location."
#~ msgstr "Instale Mapis GNOME par vierzi la posizion."

#~ msgid "No results matched search"
#~ msgstr "Nissun risultât al corispuint ae ricercje"

#~ msgid "%s"
#~ msgstr "%s"

#~ msgid "Contact deleted: “%s”"
#~ msgstr "Contat eliminât: “%s”"

#~ msgid "Select Address Book"
#~ msgstr "Selezione rubriche"

#~ msgid "— contact management"
#~ msgstr "- gjestion contats"

#~ msgid "Does %s from %s belong here?"
#~ msgstr "%s di %s isal di chest contat?"

#~ msgid "Do these details belong to %s?"
#~ msgstr "Chescj detais sono di %s?"

#~ msgid "Yes"
#~ msgstr "Sì gjo"

#~ msgid "No"
#~ msgstr "No"

#~ msgid "Suggestions"
#~ msgstr "Sugjeriment"

#~ msgid "View subset"
#~ msgstr "Mostre sotinsiemi"

#~ msgid "View contacts subset"
#~ msgstr "Mostre sotinsiemi dai contats"

#~ msgid "x-office-address-book"
#~ msgstr "x-office-address-book"

#~ msgid "January"
#~ msgstr "Zenâr"

#~ msgid "February"
#~ msgstr "Fevrâr"

#~ msgid "April"
#~ msgstr "Avrîl"

#~ msgid "June"
#~ msgstr "Jugn"

#~ msgid "July"
#~ msgstr "Lui"

#~ msgid "August"
#~ msgstr "Avost"

#~ msgid "September"
#~ msgstr "Setembar"

#~ msgid "October"
#~ msgstr "Otubar"

#~ msgid "November"
#~ msgstr "Novembar"

#~ msgid "December"
#~ msgstr "Decembar"

#~ msgid "_Change Address Book..."
#~ msgstr "_Cambie rubriche..."

#~ msgid "Personal email"
#~ msgstr "Email personâl"

#~ msgid "Twitter"
#~ msgstr "Twitter"

#~ msgid "Google Profile"
#~ msgstr "Profilo Google"

#~ msgid "Google Other Contact"
#~ msgstr "Altri contat Google"

#~ msgid "%s - Linked Accounts"
#~ msgstr "%s - account colegâts"

#~ msgid ""
#~ "Add or \n"
#~ "select a picture"
#~ msgstr ""
#~ "Zonte o\n"
#~ "selezione une imagjin "

#~ msgid "Contact Name"
#~ msgstr "Non dal contat"

#~ msgid "Email"
#~ msgstr "Email"

#~ msgid "Phone"
#~ msgstr "Telefon"

#~ msgid "You must specify a contact name"
#~ msgstr "A si scugne specificâ un non par il contat"

#~ msgid "Please select your primary contacts account"
#~ msgstr "Par plasê selezione il to account primari dai contats"
